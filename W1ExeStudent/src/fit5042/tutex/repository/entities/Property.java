/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository.entities;

import java.math.BigDecimal;
import java.text.NumberFormat;

/**
 *
 * @author Junyang
 * 
 */
//TODO Exercise 1.3 Step 1 Please refer tutorial exercise. 
public class Property {
    private int propertyID;
    private String address;
    private int numberOfBedroom;
    private int size;
    private double price;
    
    
	public Property(int propertyID, String address, int numberOfBedroom, int size, double price) {
		// auto generated when generating constructor using fields
		// why is this super here?
		// what is this for?
		super();
		this.propertyID = propertyID;
		this.address = address;
		this.numberOfBedroom = numberOfBedroom;
		this.size = size;
		this.price = price;
	}


	public int getPropertyID() {
		return propertyID;
	}


	public void setPropertyID(int propertyID) {
		this.propertyID = propertyID;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public int getNumberOfBedroom() {
		return numberOfBedroom;
	}


	public void setNumberOfBedroom(int numberOfBedroom) {
		this.numberOfBedroom = numberOfBedroom;
	}


	public int getSize() {
		return size;
	}


	public void setSize(int size) {
		this.size = size;
	}


	public double getPrice() {
		return price;
	}


	public void setPrice(double price) {
		this.price = price;
	}


	@Override
	public String toString() {
		return "Property [propertyID=" + propertyID + ", address=" + address + ", numberOfBedroom=" + numberOfBedroom
				+ ", size=" + size + ", price=" + price + "]";
	}
    
    
    
}


